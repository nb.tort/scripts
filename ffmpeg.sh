#!/bin/bash
#
# video converter for ipods
#
# made by nb.tort, under the GPL3 license.
#

function DEVICE_CHOOSE() {
    PS3="Enter a number: "

    select DEVICE in PSP iPod
    do
        case $DEVICE in
            PSP)
                read -p "Input the file location. (ex: /home/tort/Videos/pumpkinhead.mp4): " VID_INPUT
                ffmpeg -y -i "$VID_INPUT" -flags +bitexact -vcodec libx264 -profile:v baseline -level 3.0 -s 480x272 -r 29.97 -b:v 384k -acodec aac -b:a 96k -ar 48000 -f psp -strict -2 "$VID_INPUT.PSP.MP4"
                exit
                ;;
            iPod)
                read -p "Input the file location. (ex: /home/tort/Videos/pumpkinhead.mp4): " VID_INPUT
                ffmpeg -i "$VID_INPUT" -c:a aac -cutoff 15000 -ab 128k -ar 44100 -ac 1 -strict -2 -async 1 -c:v libx264 -b:v 1024k -maxrate 2048k -bufsize 2048k -r 23.81 -s 320x180 -aspect 16:9 -pix_fmt yuv420p -movflags faststart -profile:v baseline -level 13 -partitions partb8x8+partp4x4+partp8x8+parti8x8 -b-pyramid 1 -weightb 0 -8x8dct 0 -fast-pskip 1 -trellis 1 -me_method hex -flags +loop -sws_flags fast_bilinear -direct-pred 1 -sc_threshold 40 -qmin 3 -qmax 51 -threads 4 -sn -y "$VID_INPUT.iPod.MP4"
                exit
                ;;
            *)
                break
                ;;
        esac
    done
    exit
}

function CHECKS() {
    if ! command -v ffmpeg &> /dev/null
    then
        printf "ffmpeg couldn't be found\nif you're using macOS please install it via brew https://brew.sh\nif you're using linux please install it through your package manager.\n\nafter installing, rerun the script."
        exit
    else
        DEVICE_CHOOSE
    fi
}

CHECKS