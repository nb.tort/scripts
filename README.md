# nb-tort's bash scripts
This is a collection of bash scripts I have made. The repo will be populated with more scripts, below is what each script does and how to use it.

All scripts in this repository are licensed under [GPL3](LICENSE), unless otherwise stated in the beginning of the source code.

---

## ffmpeg.sh
This makes converting videos for specific devices where you can't just drag and drop the video file onto the device. Right now it only supports iPod classics and PSP.

### Notes about ffmpeg.sh
1) You will need either macOS or Linux. for Windows users, you will need to install WSL, which you can find [here](https://learn.microsoft.com/en-us/windows/wsl/install)
2) ffmpeg will need to be installed. On macOS it's preffered you use [brew](https://brew.sh) to install it. below are examples on how to install ffmpeg:

- macOS:
```brew install ffmpeg```
- Debian/Ubuntu
```sudo apt install ffmpeg```
- Fedora/RHEL/Mageia
```sudo dnf install ffmpeg```
- Arch
```sudo pacman -S ffmpeg```

When using WSL, you will have to install it the same way as you would install one of the following Linux distributions.

3) When putting in the video file you want to convert, you will have to type in it's ABSOLUTE path. If you don't understand what this means you can just drag and drop the video file into the terminal when the script asks for the video file path. This is known to work on macOS.
4) The output file will be the name of the original file with `.iPod.MP4` or `.PSP.MP4` appended at the end. You can rename this to whatever just make sure it has the `.mp4` extension at the end (case-insensitive)
5) If you want to make sure the script works before trying to convert a massive file like a movie, you can [use this test footage](http://www.nb-tort.com/media/ffmpeg_test.mp4). It's only 20 seconds long and is 54mb big.

---

## Contact:
- Discord: nonbinary.tort